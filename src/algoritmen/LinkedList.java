package algoritmen;

public class LinkedList <E> {
  private int size;
  private Node<E> head;
  
  public LinkedList() {
	  size = 0;
	  head = null;
  }
  public LinkedList(E element) {
	size = 1;
	head = new Node<E>(element);
	  }
  
  public int getSize() {
	return size;  
  }
  
  public Node<E> getHead(){
	  return head;
  }
  
  public void prepend(E element) {
	 Node<E> temp = new Node<E>(element, head);
	 head = temp;
	 size++; 
  }
}
